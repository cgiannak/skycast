﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using SkyCastApp.Common;

namespace SkyCastApp.Maps
{
    public class CoordinateInformation : ICoordinateInformation
    {
        private const string APIURL = @"https://maps.googleapis.com/maps/api/geocode/json?";

        public IHttpWebServiceHandler geocodingHandler => new HttpWebServiceHandler(APIURL);

        public Point GetCoordinateInformation(string location)
        {
            GMapsRequestParameters parameters = new GMapsRequestParameters
            {
                Address = location
            };

            string rawResponseData = geocodingHandler.GetServiceResponse(parameters);
            RootObject result = JsonConvert.DeserializeObject<RootObject>(rawResponseData);

            if (result == null || result.results == null || result.results.Count == 0)
            {
                return new Point(200, 200);
            }
            Point coordinates = new Point(result.results[0].geometry.location.lat,
                                          result.results[0].geometry.location.lng);
            return coordinates;
        }
    }
}