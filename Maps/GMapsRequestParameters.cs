﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SkyCastApp.Common;

namespace SkyCastApp.Maps
{
    public class GMapsRequestParameters : IRequestParameters
    {
        private const string APIKEY = @"AIzaSyDru9u0stKRpTsA-PJzzieAcbkl_x8oGvk";

        public string Address { get; set; }

        public string GetQueryString()
        {
            return $"address={Address}&key={APIKEY}";
        }
    }
}