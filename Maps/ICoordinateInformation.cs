﻿using System.Windows;

namespace SkyCastApp.Maps
{
    public interface ICoordinateInformation
    {
        /// <summary>
        /// Converts a user-entered location to coordinates.
        /// </summary>
        /// <param name="location">The user entered location.</param>
        /// <returns>The coordinate information.</returns>
        Point GetCoordinateInformation(string location);
    }
}
