﻿using System;
using System.Web.Http;
using System.Windows;
using SkyCastApp.Common;
using SkyCastApp.Forecast;
using SkyCastApp.Maps;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace SkyCastApp.Controllers
{
    public class ForecastController : ApiController
    {
        public ICoordinateInformation CoordinateInformation { get; set; } = new CoordinateInformation();

        public IForecastInformation ForecastInformation { get; set; } = new ForecastInformation();

        public IHttpActionResult GetCurrentForecast(string location)
        {
            if (string.IsNullOrWhiteSpace(location))
            {
                return BadRequest("Empty location.");
            }

            // Get coordinates from Gmaps API
            Point coordinates = CoordinateInformation.GetCoordinateInformation(location);

            if (coordinates.X > 90 || coordinates.Y > 180)
            {
                // Invalid coordinates, return empty string;
                return BadRequest("Invalid location.");
            }
            // Get Forecast from Forecast.io API
            // Get t-5, t+5 days, convert and send back
            DateTime currentTime = DateTime.UtcNow;
            WeatherResponse response = new WeatherResponse
            {
                Days = new List<WeatherInfo>()
            };

            for (int i = -5; i <= 5; i++)
            {
                DateTime date = currentTime.AddDays(i);
                string dayOfWeek = date.DayOfWeek.ToString();
                DateTimeOffset offset = new DateTimeOffset(date);
                long unixTimeStamp = offset.ToUnixTimeSeconds();
                ForecastResponse forecast = ForecastInformation.GetForecastInformation(coordinates, unixTimeStamp);

                response.Days.Add(
                    new WeatherInfo
                    {
                        MaxTemperature = forecast.daily.data[0].temperatureMax,
                        MinTemperature = forecast.daily.data[0].temperatureMin,
                        day = dayOfWeek
                    });
            }

            string serializedResponse = JsonConvert.SerializeObject(response);
            return Ok(serializedResponse);
        }
    }
}