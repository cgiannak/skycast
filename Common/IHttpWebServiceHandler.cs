﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkyCastApp.Common
{
    public interface IHttpWebServiceHandler
    {
        string GetServiceResponse(IRequestParameters parameters);
    }
}
