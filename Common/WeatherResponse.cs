﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SkyCastApp.Common
{
    public class WeatherResponse
    {
        public IList<WeatherInfo> Days;
    }

    public class WeatherInfo
    {
        public double MaxTemperature;
        public double MinTemperature;
        public string day;
    }
}