﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace SkyCastApp.Common
{
    public class HttpWebServiceHandler : IHttpWebServiceHandler
    {
        private string ServiceUrl;

        public HttpWebServiceHandler(string serviceUrl)
        {
            ServiceUrl = serviceUrl;
        }

        public string GetServiceResponse(IRequestParameters parameters)
        {
            string fullUrl = string.Concat(ServiceUrl, parameters.GetQueryString());
            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(fullUrl);
            httpWebRequest.Method = WebRequestMethods.Http.Get;
            HttpWebResponse response = httpWebRequest.GetResponse() as HttpWebResponse;

            string rawResponse = string.Empty;

            using (Stream responseStream = response.GetResponseStream())
            {
                StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                rawResponse = reader.ReadToEnd();
            }

            return rawResponse;
        }
    }
}