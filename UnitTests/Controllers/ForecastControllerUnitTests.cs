﻿using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Results;
using System.Windows;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SkyCastApp.Controllers;
using SkyCastApp.Forecast;

namespace SkyCastApp.UnitTests.Controllers
{
    [TestClass]
    public class ForecastControllerUnitTests
    {
        [TestMethod]
        public void GetCurrentForecast_Correct_Location()
        {
            ForecastController controller = new ForecastController
            {
                ForecastInformation = new UnitTestForecastInformation()
                {
                    ForecastResponse = GetDummyForecastResponse()
                },
                CoordinateInformation = new UnitTestCoordinateInformation()
                {
                    MapInformation = new Dictionary<string, Point>() { { "Athens", new Point(5, 6) } }
                }
            };

            IHttpActionResult result = controller.GetCurrentForecast(string.Empty);
            Assert.IsInstanceOfType(result, typeof(OkNegotiatedContentResult<string>));

            OkNegotiatedContentResult<string> dataResponse = result as OkNegotiatedContentResult<string>;

            Assert.IsNotNull(dataResponse.Content);
        }

        [TestMethod]
        public void GetCurrentForecast_Empty_Location()
        {
            ForecastController controller = new ForecastController
            {
                ForecastInformation = new UnitTestForecastInformation()
                {
                    ForecastResponse = GetDummyForecastResponse()
                },
                CoordinateInformation = new UnitTestCoordinateInformation()
                {
                    MapInformation = new Dictionary<string, Point>() { { "Athens", new Point(5, 6) } }
                }
            };

            IHttpActionResult result = controller.GetCurrentForecast(string.Empty);
            Assert.IsInstanceOfType(result, typeof(BadRequestErrorMessageResult));

            BadRequestErrorMessageResult dataResponse =  result as BadRequestErrorMessageResult;
            Assert.AreEqual(dataResponse.Message, "Empty location.");
        }


        private ForecastResponse GetDummyForecastResponse()
        {
            ForecastResponse dummyResponse = new ForecastResponse();
            Datum3 datum3 = new Datum3();
            datum3.temperatureMax = 10;
            datum3.temperatureMin = 5;

            dummyResponse.daily = new Daily();
            dummyResponse.daily.data = new List<Datum3>();
            dummyResponse.daily.data.Add(datum3);

            return dummyResponse;
        }
    }
}