﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Windows;
using SkyCastApp.Maps;

namespace SkyCastApp.UnitTests.Controllers
{
    public class UnitTestCoordinateInformation : ICoordinateInformation
    {
        public Dictionary<string, Point> MapInformation { get; set; } = new Dictionary<string, Point>();

        public Point GetCoordinateInformation(string location)
        {
            Point coordinates = new Point(0, 0);
            if (MapInformation.TryGetValue(location, out coordinates))
            {
                return coordinates;
            }

            return new Point(0, 0);
        }
    }
}