﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Windows;
using SkyCastApp.Forecast;

namespace SkyCastApp.UnitTests
{
    public class UnitTestForecastInformation : IForecastInformation
    {
        public ForecastResponse ForecastResponse { get; set; } = null;
        public ForecastResponse GetForecastInformation(Point coordinates, double time)
        {
            return ForecastResponse;
        }
    }
}