﻿$(document).ready(function () {

    retrieveSearchHistory();
    $("#mysearch").keyup(function (event) {
        if (event.keyCode == 13) {
            $("#mybutton").click();
        }
    });

    $("#mybutton").click(function () {

        var ctx = document.getElementById("myChart").getContext("2d");
        ctx.clearRect(0, 0, 0, 0);
        var searchTerm = $("#mysearch").val();
        $("#mysearch").val("");

        pushSearch(searchTerm);
        retrieveSearchHistory();
        $.get("/api/forecast/?location=" + searchTerm,
            function (data, status) {
                renderGraph(data)
            });
    });
});

function renderGraph(data)
{
    var weatherData = JSON.parse(data);
    var weatherDataMin = weatherData.Days.map(function (o) { return o.MinTemperature; });
    var weatherDataMax = weatherData.Days.map(function (o) { return o.MaxTemperature; });
    var dayLabels = weatherData.Days.map(function (o) { return o.day; });

    var barChartData = {
    labels: dayLabels,
    datasets: [{
        label: 'Min Temperature',
        backgroundColor: 'rgba(255, 99, 132, 0.2)',
        borderWidth: 1,
        data: weatherDataMin,
    }, {
        label: 'Max Temperature',
        backgroundColor: 'rgba(54, 162, 235, 0.2)',
        borderWidth: 1,
        data: weatherDataMax,
    }]
    };

    var ctx = document.getElementById("myChart").getContext("2d");
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: barChartData,
        options: {
            responsive: true,
            legend: {
                position: 'top',
            },
            title: {
                display: true,
                text: '10 days Weather (F)'
            }
        }
    });

}

function retrieveSearchHistory()
{
    var history = getHistory();
    
    if (history === null) {
        return;
    }

    history = history.slice(1).slice(-5);
    $("#mysearch").autocomplete({
        source: history,
        minLength: 0,
    }).focus(function () {
        $(this).autocomplete("search");
    });
}

function pushSearch(searchTerm)
{
    var history = JSON.parse(localStorage.getItem("history"));
    if (history === null) {
        history = [];
    }

    history.push(searchTerm);
    localStorage.setItem("history", JSON.stringify(history));
}

function getHistory()
{
    var history = localStorage.getItem("history");
    if (history != null)
    {
        return JSON.parse(localStorage.getItem("history"));
    }

    return '';
}