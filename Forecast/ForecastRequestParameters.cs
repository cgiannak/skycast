﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SkyCastApp.Common;

namespace SkyCastApp.Forecast
{
    public class ForecastRequestParameters : IRequestParameters
    {
        private const string APIKEY = @"567eb1a0a59bd018000770af1048e8d0";

        public double Latitude { get; set; }
        public double Longitude { get; set; }

        /// <summary>
        /// The time reference in UNIX Epoch
        /// </summary>
        public double Time { get; set; }

        public string GetQueryString()
        {
            return $"{APIKEY}/{Latitude},{Longitude},{Time}?exclude=hourly,currently,minutely,alerts,flags";
        }
    }
}