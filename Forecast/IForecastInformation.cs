﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SkyCastApp.Forecast
{
    public interface IForecastInformation
    {
        ForecastResponse GetForecastInformation(Point coordinates, double time);
    }
}
