﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Windows;
using Newtonsoft.Json;
using SkyCastApp.Common;

namespace SkyCastApp.Forecast
{
    public class ForecastInformation : IForecastInformation
    {
        private const string APIURL = @"https://api.darksky.net/forecast/";

        private IHttpWebServiceHandler forecastHandler => new HttpWebServiceHandler(APIURL);

        public ForecastResponse GetForecastInformation(Point coordinates, double time)
        {
            ForecastRequestParameters parameters = new ForecastRequestParameters
            {
                Latitude = coordinates.X,
                Longitude = coordinates.Y,
                Time = time
            };

            string rawData = forecastHandler.GetServiceResponse(parameters);
            ForecastResponse response = JsonConvert.DeserializeObject<ForecastResponse>(rawData);

            return response;
        }
    }
}